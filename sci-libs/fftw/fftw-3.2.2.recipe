SUMMARY="Fast C library for the Discrete Fourier Transform"
DESCRIPTION="
FFTW is a C subroutine library for computing the discrete Fourier transform \
(DFT) in one or more dimensions, of arbitrary input size, and of both real and \
complex data (as well as of even/odd data, i.e. the discrete cosine/sine \
transforms or DCT/DST). We believe that FFTW, which is free software, should \
become the FFT library of choice for most applications.
Our benchmarks, performed on on a variety of platforms, show that FFTW's \
performance is typically superior to that of other publicly available FFT \
software, and is even competitive with vendor-tuned codes. In contrast to \
vendor-tuned codes, however, FFTW's performance is portable: the same program \
will perform well on most architectures without modification. Hence the name, \
FFTW, which stands for the somewhat whimsical title of Fastest Fourier \
Transform in the West.
"
HOMEPAGE="http://www.fftw.org/"
SRC_URI="http://www.fftw.org/fftw-3.2.2.tar.gz"
CHECKSUM_SHA256="6aa7ae65ee49eb99004f15899f9bb77f54759122f1a350041e81e096157d768f"
LICENSE="GNU GPL v2"
COPYRIGHT="
	2003, 2007-2008 Matteo Frigo
	2003, 2007-2008 Massachusetts Institute of Technology
	"
REVISION="1"
ARCHITECTURES="x86 x86_gcc2"
SECONDARY_ARCHITECTURES="x86_gcc2 x86"

PROVIDES="
	lib:libfftw$secondaryArchSuffix = $portVersion
	cmd:fftw_wisdom
	cmd:fftw_wisdom_to_conf
	"

REQUIRES="
        haiku$secondaryArchSuffix
	"

BUILD_REQUIRES="
        haiku${secondaryArchSuffix}_devel
	"

BUILD_PREREQUIRES="
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:make
	"

BUILD()
{
	runConfigure ./configure
	make
}

INSTALL()
{
	make install
	prepareInstalledDevelLib libfftw3
	packageEntries devel $developDir
}

PROVIDES_devel="
	devel:libfftw$secondaryArchSuffix = $portVersion
	"
REQUIRES_devel="
	lib:libfftw$secondaryArchSuffix == $portVersion base
	"
