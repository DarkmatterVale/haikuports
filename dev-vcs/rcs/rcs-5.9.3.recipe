SUMMARY="Revision Control System"
DESCRIPTION="The Revision Control System (RCS) manages multiple revisions \
of files.  RCS automates the storing, retrieval, logging, identification, \
and merging of revisions.  RCS is useful for text that is revised \
frequently, including source code, programs, documentation, graphics, \
papers, and form letters.

RCS is (mostly) deprecated now. Unless you have an existing RCS repository \
that you need to access, using Subversion or Git is preferred."
HOMEPAGE="http://www.gnu.org/software/rcs/"
COPYRIGHT="1986-2005 Free Software Foundation, Inc."
LICENSE="GNU GPL v1
	GNU LGPL v2"
SRC_URI="https://ftp.gnu.org/gnu/rcs/rcs-5.9.3.tar.xz"
CHECKSUM_SHA256="68b0d1c1b8657766cd7dc78e10564ce4836220e64665fb35fbe74dbc1dff5b47"
REVISION="1"
ARCHITECTURES="x86 ?x86_64"
if [ $effectiveTargetArchitecture != x86_gcc2 ]; then
    # x86_gcc2 is fine as primary target architecture as long as we're building
    # for a different secondary architecture.
    ARCHITECTURES="$ARCHITECTURES x86_gcc2"
else
    ARCHITECTURES="$ARCHITECTURES !x86_gcc2"
fi
SECONDARY_ARCHITECTURES="x86"

PROVIDES="
	rcs = $portVersion compat >= 5.9
	cmd:ci = $portVersion compat >= 5.9
	cmd:co = $portVersion compat >= 5.9
	cmd:ident = $portVersion compat >= 5.9
	cmd:merge = $portVersion compat >= 5.9
	cmd:rcs = $portVersion compat >= 5.9
	cmd:rcsclean = $portVersion compat >= 5.9
	cmd:rcsdiff = $portVersion compat >= 5.9
	cmd:rcsmerge = $portVersion compat >= 5.9
	cmd:rlog = $portVersion compat >= 5.9
	"
REQUIRES="
	haiku$secondaryArchSuffix
	cmd:ed
	cmd:awk
	cmd:diff
	"
BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel
	"
BUILD_PREREQUIRES="
	cmd:make
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	
	cmd:ed
	cmd:awk
	cmd:diff
	"

BUILD()
{
	runConfigure --omit-dirs binDir ./configure --bindir=$prefix/bin
	make $jobArgs
}

INSTALL()
{
	make install
}
